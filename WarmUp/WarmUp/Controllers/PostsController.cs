﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarmUp.Models;

namespace WarmUp.Controllers
{
    
    public class PostsController : Controller
    {
        public readonly Warm_Up_ChallengeContext db;

        public PostsController(Warm_Up_ChallengeContext context)
        {
            db = context;
        }

        //GET
        
        public IActionResult Index()
        {
            var Posts = db.Posts.ToList();
            Posts.Sort((x, y)=> y.FechaCreacion.CompareTo(x.FechaCreacion));
            return View(Posts);
        }

        //Get-Details
        [Route("/{controller}/{id:int}")]
        public IActionResult Details(int? id)
        {
            var consulta = db.Posts.Where(p => p.Id == id).Count();
            if (consulta == 0)
            {
                return NotFound();
            }
            else
            {
                var post = db.Posts.Where(p => p.Id == id).FirstOrDefault();
                return View(post);
            }
        }

        //GET-Create
        [Route("/{controller}")]
        public IActionResult Create()
        {
            return View();
        }

        //GET-Delete
        
        public IActionResult Delete(int? id)
        {
            
            if (id == null || id == 0)
            {
                return NotFound();
            }
            else
            {
                var obj = db.Posts.Where(p => p.Id == id).FirstOrDefault();
                return View(obj);
            }
        }

        //GET-Update
        public IActionResult Update(int? id)
        {

            if (id == null || id == 0)
            {
                return NotFound();
            }
            else
            {
                var obj = db.Posts.Where(p => p.Id == id).FirstOrDefault();
                return View(obj);
            }
        }

        //POST-Create
        [HttpPost]
        [Route("/{controller}/")]
        public IActionResult Create(Post obj)
        {
            obj.FechaCreacion = DateTime.Now;
            db.Posts.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //POST-Delete
        [HttpPost]
        public IActionResult DeletePost(int id)
        {
            var post = db.Posts.Where(p => p.Id == id).Count();
            if (post == 0)
            {
                return NotFound();
            }
            else
            {
                var delPost = db.Posts.Where(p => p.Id == id).FirstOrDefault();
                db.Posts.Remove(delPost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        //POST-Update
        [HttpPost]
        public IActionResult Update(Post obj)
        {
            obj.FechaCreacion = DateTime.Now;
            db.Posts.Update(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
