﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace WarmUp.Models
{
    public partial class Post
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Título no puede estar vacío")]
        [DisplayName("Título")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "Contenido no puede estar vacío")]
        public string Contenido { get; set; }
        public string Imagen { get; set; }
        [Required]
        [DisplayName("Fecha de creación")]
        [DataType(DataType.Date)]
        public DateTime FechaCreacion { get; set; }
        
        [Required(ErrorMessage = "Categoría no puede estar vacío")]
        public string Categoría { get; set; }
    }
}
